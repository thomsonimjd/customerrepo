
## Requirements

The project requires [Java 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) or
higher.

The project makes use of Maven and uses

### Run the tests

- Run unit tests only

  ```console
  $ mvn test
  ```
